package com.richinnovation.worktofeed.data.webservice

import com.google.gson.GsonBuilder
import com.localdb.test.utils.LConstant
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceGenerator {


    fun <S> creatService(serviceClass: Class<S>): S {

        val gson = GsonBuilder().setLenient().create()

        //OkHttpClient client = new OkHttpClient();
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .build()


        val retrofit = Retrofit.Builder().baseUrl(LConstant.BASE_URL).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        return retrofit.create(serviceClass)
    }

}
