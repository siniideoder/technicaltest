package com.localdb.test.data.local.roomdb;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

public class OfflineDataRepository {
    private OfflineDataDao mOfflineDataDao;
    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public OfflineDataRepository(Application application) {
        OfflineRoomDatabase db = OfflineRoomDatabase.getDatabase(application);
        mOfflineDataDao = db.OfflineDaoAssess();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public void getApiResponse(String apiName,ApiResponseCallback apiResponseCallback) {
        AsyncGetApiResponse asyncGetApiResponse =  new AsyncGetApiResponse(apiResponseCallback);
        asyncGetApiResponse.execute(apiName);
    }

     public void deleteTable(){
         OfflineRoomDatabase.databaseWriteExecutor.execute(() -> {
                mOfflineDataDao.deleteTable();
            });
     }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void insert(OfflineEntity offlineEntity) {

        AsyncInsertApiResponse asyncInsertApiResponse = new AsyncInsertApiResponse(offlineEntity.getApiName(),offlineEntity.getApiResponse());
        asyncInsertApiResponse.execute();



//        if (checkIfApiNameAlreadyExist(offlineEntity)){
//            OfflineRoomDatabase.databaseWriteExecutor.execute(() -> {
//                mOfflineDataDao.insertOffLineData(offlineEntity);
//            });
//        }else {
//            OfflineRoomDatabase.databaseWriteExecutor.execute(() -> {
//                mOfflineDataDao.updateOffLineData(offlineEntity.getApiName(),offlineEntity.getApiResponse());
//            });
//        }

    }

//    private boolean checkIfApiNameAlreadyExist(OfflineEntity offlineEntity) {
////        if (mOfflineDataDao.getApiResponse(offlineEntity.getApiName())==null){
////            return true;
////        }
////        return false;
////    }

    public   interface  ApiResponseCallback{
        void onFetchListCompleted(List<OfflineEntity> offlineEntities);
        void onError(String error);
    }


    class AsyncGetApiResponse extends AsyncTask<String,String,List<OfflineEntity>>{
        ApiResponseCallback apiResponseCallback;

        public AsyncGetApiResponse(ApiResponseCallback apiResponseCallback) {
            this.apiResponseCallback = apiResponseCallback;
        }

        @Override
        protected List<OfflineEntity> doInBackground(String... params) {
            String apiName= params[0];
            return mOfflineDataDao.getApiResponse(apiName);
        }

        @Override
        protected void onPostExecute(List<OfflineEntity> s) {
            super.onPostExecute(s);
            if (s!=null && s.size()>0){
                apiResponseCallback.onFetchListCompleted(s);
            }else {
                apiResponseCallback.onError("No data");
            }

        }
    }

    class AsyncInsertApiResponse extends AsyncTask<Void,Void,Void>{
        String apiName;
        String apiResponse;

        public AsyncInsertApiResponse(String apiName,String apiResponse) {
            this.apiName = apiName;
            this.apiResponse=apiResponse;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            List<OfflineEntity> list =  mOfflineDataDao.getApiResponse(apiName);
            if (list==null || list.size()==0){
                OfflineEntity offlineEntity = new OfflineEntity(apiName,apiResponse);
                mOfflineDataDao.insertOffLineData(offlineEntity);
            }else {
                mOfflineDataDao.updateOffLineData(apiName,apiResponse);
            }
            return null;
        }
    }


}
