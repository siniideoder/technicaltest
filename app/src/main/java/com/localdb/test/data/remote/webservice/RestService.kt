package com.richinnovation.worktofeed.data.webservice


import com.localdb.test.data.remote.model.contact.ContactBean
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface RestService {
    @GET("users")
    fun getContact(): Call<ResponseBody>
}