package com.localdb.test.data.local.roomdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface OfflineDataDao {
    @Insert
    void insertOffLineData(OfflineEntity offlineEntity);

    @Query("Select * from offline_api_table where api_name= :apiName")
    List<OfflineEntity> getApiResponse(String apiName);

    @Query("UPDATE offline_api_table SET api_response = :apiResponse WHERE api_name =:apiName")
    void updateOffLineData(String apiName, String apiResponse);

    @Query("DELETE  from offline_api_table")
    void deleteTable();
}
