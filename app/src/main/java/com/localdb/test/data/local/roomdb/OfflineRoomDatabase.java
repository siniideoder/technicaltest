package com.localdb.test.data.local.roomdb;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {OfflineEntity.class}, version = 1, exportSchema = false)
public abstract class OfflineRoomDatabase extends RoomDatabase {
    public abstract OfflineDataDao OfflineDaoAssess();

    private static volatile OfflineRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static OfflineRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (OfflineRoomDatabase.class) {
                if (INSTANCE == null) {
                    Log.e("DTACONTEXT",""+context);
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            OfflineRoomDatabase.class, "word_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}