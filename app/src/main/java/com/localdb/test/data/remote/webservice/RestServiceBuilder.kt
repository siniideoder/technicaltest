package com.richinnovation.worktofeed.data.webservice

object RestServiceBuilder {

    private var service: RestService? = null

    val apiService: RestService
        get() {
            service = ServiceGenerator.creatService(RestService::class.java)
            return service!!
        }

}
