package com.localdb.test.data.local.roomdb;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "offline_api_table")
public class OfflineEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "api_name")
    private String mApiName;

    @ColumnInfo(name="api_response")
    private String mApiResponse;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OfflineEntity(String mApiName, String mApiResponse) {
        this.mApiName = mApiName;
        this.mApiResponse = mApiResponse;
    }

    public String getApiName() {
        return mApiName;
    }

    public void setApiName(String mApiName) {
        this.mApiName = mApiName;
    }

    public String getApiResponse() {
        return mApiResponse;
    }

    public void setApiResponse(String mApiResponse) {
        this.mApiResponse = mApiResponse;
    }
}
