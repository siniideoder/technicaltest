package com.localdb.test.ui.module.home

import com.localdb.test.data.remote.model.contact.ContactBean

interface HomeView {
    interface View{
        fun onSuccess(contactBean: ArrayList<ContactBean>?)
        fun onError()

    }
    interface Presenter{
        fun getContact()

    }
}