package com.localdb.test.ui.module.home

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.localdb.test.data.local.roomdb.OfflineDataRepository
import com.localdb.test.data.local.roomdb.OfflineEntity
import com.localdb.test.data.remote.model.contact.ContactBean
import com.localdb.test.utils.LConstant
import com.localdb.test.utils.Utils
import com.localdb.test.utils.application.LApplicationClass
import com.richinnovation.worktofeed.data.webservice.RestServiceBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.lang.reflect.Type

class HomePresenter(var mActivity: Activity, var mView: HomeView.View) : HomeView.Presenter {

    override fun getContact() {
        LApplicationClass.getOffLineDataRepository()
            .getApiResponse(LConstant.CONTACT, object :
                OfflineDataRepository.ApiResponseCallback {
                override fun onFetchListCompleted(offlineEntities: List<OfflineEntity>?) {
                    if (offlineEntities != null && offlineEntities.isNotEmpty()) {
                        val responseStr = offlineEntities[0].apiResponse
                        Log.d("RESPONSE", "" + responseStr)
                        if (!responseStr.isEmpty()) {
                            val userListType: Type =
                                object : TypeToken<ArrayList<ContactBean?>?>() {}.type
                            val contactList: ArrayList<ContactBean> =
                                Gson().fromJson(responseStr, userListType)
                            mView.onSuccess(contactList)
                        } else Log.d("DATA", "EMPTY")
                        if (Utils.isNetworkAvailable(mActivity))
                            getFromRemote()
                    }
                }

                override fun onError(error: String?) {
                    if (Utils.isNetworkAvailable(mActivity))
                        getFromRemote()
                    else mView.onError()
                }
            })
    }

    private fun getFromRemote() {
        RestServiceBuilder.apiService.getContact()
            .enqueue(object : retrofit2.Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        val responseStr = response.body()?.string()
                        println(responseStr)
                        val offlineEntity =
                            OfflineEntity(LConstant.CONTACT, responseStr)
                        LApplicationClass.getOffLineDataRepository().insert(offlineEntity)

                        val userListType: Type =
                            object : TypeToken<ArrayList<ContactBean?>?>() {}.type
                        val contactList: ArrayList<ContactBean> =
                            Gson().fromJson(responseStr, userListType)
                        mView.onSuccess(contactList)

                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }
            })
    }

}