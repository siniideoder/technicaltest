package com.localdb.test.ui.module.home

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.localdb.test.R
import com.localdb.test.data.remote.model.contact.ContactBean
import kotlinx.android.synthetic.main.activity_home_layout.*

class HomeActivity : Activity(), HomeView.View {
    private var mPresenter: HomePresenter? = null
    private var mAdapter: HomeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_layout)
        init();

        mPresenter?.getContact()
    }

    private fun init() {
        m_recyclerview.layoutManager = LinearLayoutManager(this)
        mPresenter = HomePresenter(this, this)
    }

    override fun onSuccess(contactBean: ArrayList<ContactBean>?) {
        if (contactBean != null) {
            no_internet.visibility = View.GONE
            m_recyclerview.visibility = View.VISIBLE
            mAdapter = HomeAdapter(this, contactBean)
            m_recyclerview.adapter = mAdapter
        }
    }

    override fun onError() {
        no_internet.visibility = View.VISIBLE
        m_recyclerview.visibility = View.GONE
    }
}