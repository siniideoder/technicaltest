package com.localdb.test.ui.module.details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.localdb.test.R
import com.localdb.test.data.remote.model.contact.ContactBean
import com.localdb.test.ui.module.details.location.LocationActivity
import com.localdb.test.utils.LConstant
import kotlinx.android.synthetic.main.activity_cattle_details_layout.*

class ContactDetails : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cattle_details_layout)
        init()
    }

    private fun init() {
        if (intent.hasExtra(LConstant.DETAIL)) {
            val contactDetails = intent.getSerializableExtra(LConstant.DETAIL) as ContactBean
            setDetails(contactDetails)
        }
    }

    private fun setDetails(contactDetails: ContactBean) {
        tv_user_name.text = contactDetails.username
        tv_name_tab.text = contactDetails.name
        tv_email.text = contactDetails.email
        tv_contact.text = contactDetails.phone
        tv_website.text = contactDetails.website

        val stringBuilder = StringBuilder()
        stringBuilder.append(contactDetails.address.city + ", ")
            .append(contactDetails.address.street + ", ")
            .append(contactDetails.address.suite + ", ")
            .append(contactDetails.address.city + ", ")
            .append(contactDetails.address.zipcode + ", ")
        tv_address.text = stringBuilder.toString()

        val companyBuilder = StringBuilder()
        companyBuilder.append(contactDetails.company.name + ", ")
            .append(contactDetails.company.catchPhrase + ", ")
            .append(contactDetails.company.bs)

        tv_company_details.text = companyBuilder.toString()

        btn_view_location.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            intent.putExtra("lat", contactDetails.address.geo.lat)
            intent.putExtra("long", contactDetails.address.geo.lng)
            startActivity(intent)
        }
    }
}