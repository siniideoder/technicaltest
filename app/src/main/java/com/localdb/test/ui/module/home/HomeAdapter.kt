package com.localdb.test.ui.module.home

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.localdb.test.R
import com.localdb.test.data.remote.model.contact.ContactBean
import com.localdb.test.ui.module.details.ContactDetails
import com.localdb.test.ui.module.home.HomeAdapter.SubjectViewHolder
import com.localdb.test.utils.LConstant
import kotlinx.android.synthetic.main.contact_item_layout.view.*

class HomeAdapter(
    private val mContext: Context,
    private var contactList: List<ContactBean>
) :
    RecyclerView.Adapter<SubjectViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        val v =
            LayoutInflater.from(mContext).inflate(R.layout.contact_item_layout, parent, false)
        return SubjectViewHolder(v)
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        holder.mName?.text = contactList[position].name
        holder.mContact?.text = contactList[position].phone
        holder.parentLayout?.setOnClickListener {
            val intent = Intent(mContext, ContactDetails::class.java)
            intent.putExtra(LConstant.DETAIL, contactList[position])
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    class SubjectViewHolder(itemView: View) : ViewHolder(itemView) {
        var mName: TextView? = itemView.tv_name
        var mContact: TextView? = itemView.tv_contact
        var parentLayout: RelativeLayout? = itemView.parentLayout
    }

}