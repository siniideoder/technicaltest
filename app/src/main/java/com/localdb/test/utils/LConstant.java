package com.localdb.test.utils;

import org.jetbrains.annotations.NotNull;

public interface LConstant {
    public static String BASE_URL = "https://jsonplaceholder.typicode.com/";
    public static String CONTACT ="/users";
    public static String DETAIL="details";

}
