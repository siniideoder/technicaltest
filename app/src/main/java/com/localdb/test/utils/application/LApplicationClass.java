package com.localdb.test.utils.application;

import android.app.Application;
import android.content.ContextWrapper;
import android.util.Log;

import com.localdb.test.data.local.roomdb.OfflineDataRepository;


public class LApplicationClass extends Application {

    private static LApplicationClass instance = null;

    public static LApplicationClass get() {
        if (instance == null)
            return new LApplicationClass();
        else
            return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }
    public static OfflineDataRepository getOffLineDataRepository(){
        Log.e("INSTANCE",""+LApplicationClass.get());
        return new OfflineDataRepository(LApplicationClass.get());
    }
}

